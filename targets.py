import importlib
import sys

from utils import *
from pub_config import *

def parse_args(*args):
    arg_dict = {}
    pairs = args[0]
    for pair in pairs:
        (key, val) = pair.split('=')
        arg_dict[key] = val

    return arg_dict

def clone_basic_themes():
    for theme in basic_themes:
        key = theme.keys()[0]
        fetch_and_build(theme[key])

def clone_exp_repos():
    for repo in exp_repos:
        key = repo.keys()[0]
        fetch_and_build(repo[key])

def clone_exporters():
    for exporter in exporters:
        key = exporter.keys()[0]
        fetch_and_build(exporter[key])

def wget_orgs():
    for org_mode in org_modes:
        key = org_mode.keys()[0]
        wget_and_untar(key, org_mode[key])

def build():
    module = None
    try:
        module = importlib.import_module("make_config")
    except ImportError:
        print "module make_config not found"

    if module is not None:
        theme = module.theme
        exporter = module.exporter
    else:
        theme = "default"
        exporter = "org-exporter-9"

    copy_exporter(exporter)
    link_orgs(org_modes)
    copy_theme(theme)


def clean():
    clean_local(org_modes)

def infra_clean():
    clean_infra()

if __name__ == '__main__':
    arguments = parse_args(sys.argv[1:])
    target = arguments['target']
    if target == "clone-exporters":
        clone_exporters()
    elif target == "clone-basic-themes":
        clone_basic_themes()
    elif target == "clone-exp-repos":
        clone_exp_repos()
    elif target == "wget-orgs":
        wget_orgs()
    elif target == "build":
        build()
    elif target == "clean":
        clean()
    elif target == "clean-infra":
        infra_clean()
