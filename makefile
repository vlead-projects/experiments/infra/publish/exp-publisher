clone-basic-infra: wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-exp-infra: clone-basic-infra clone-exp-repos
	echo "exp infrastructure put in place"

clone-basic-themes:
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=clone-basic-themes)

clone-exporters:
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=clone-exporters)

wget-orgs:
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=wget-orgs)

clone-exp-repos:
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=clone-exp-repos)

clean-build:
	rm -rf build

build: clean-build
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=build)
	emacs --script elisp/publish.el

build-with-basic-infra: clone-basic-infra build
	echo "build with basic infra"

build-with-full-infra: clone-exp-infra build
	echo "build with full infra"

clean:
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=clean)

clean-infra:
	(export PYTHONPATH=`pwd`; python exp-publisher/targets.py target=clean-infra)
