import os

INFRA_LOC = os.path.expanduser("~/tmp/experiments/infra")

exporters = [{"org-exporter-9": {"url": "https://gitlab.com/vlead-projects/experiments/infra/publish/org-exporter-9.git",
                                 "branch": "master",
                                 "make_required": 0}}]


basic_themes = [{"default": {"url": "https://gitlab.com/vlead-projects/html-themes/default.git",
                            "branch": "master",
                            "make_required": 0}},
                {"readtheorg": {"url": "https://gitlab.com/vlead-projects/html-themes/readtheorg.git",
                                "branch": "master",
                                "make_required": 0}},
                {"vlead": {"url": "https://gitlab.com/vlead-projects/html-themes/vlead.git",
                           "branch": "master",
                           "make_required": 0}}]

exp_repos = [ {"parsers": {"url": "https://gitlab.com/vlead-projects/experiments/infra/parsers.git",
                          "branch": "master",
                          "make_required": 1}},
              {"linkers": {"url": "https://gitlab.com/vlead-projects/experiments/infra/linkers.git",
                           "branch": "master",
                           "make_required": 1}},
              {"renderers": {"url": "https://gitlab.com/vlead-projects/experiments/infra/renderers.git",
                            "branch": "master",
                            "make_required": 1}},
              {"exp-templates": {"url": "https://gitlab.com/vlead-projects/experiments/infra/publish/exp-templates.git",
                                 "branch": "master",
                                 "make_required": 1}},
              {"landing-page-style": {"url": "https://gitlab.com/vlead-projects/experiments/infra/styles/landing-page-style.git",
                                      "branch": "master",
                                      "make_required": 1}}
            ]

exp_themes = {"exp_theme": {"templates": "exp-templates",
                            "processes": ["parsers", "linkers", "renderers"],
                            "styles": ["landing-page-style"]
                            }
             }

org_modes = [{"org-9": "https://orgmode.org/org-9.1.13.tar.gz"}]
