import os
import importlib
import subprocess

from pub_config import *


def get_home_dir():
    dir_path = os.getcwd()


def make_infra_path():
    execute_command("mkdir -p %s" % (INFRA_LOC))


def get_dir_of_repo(url):
    return url.split("/")[-1:][0].split(".")[0]


def get_repo_full_path(repo):
    repo_name = get_dir_of_repo(repo)
    repo_path = INFRA_LOC + "/" + repo_name
    return repo_path


def repo_exists(url):
    full_repo_path = get_repo_full_path(url)
    return os.path.isdir(full_repo_path)


def get_proxy():
    module = None
    try:
        module = importlib.import_module("make_config")
    except ImportError:
        print "module make_config not found"

    if module is not None:
        http_proxy=module.http_proxy
    else:
        http_proxy=""

    return http_proxy


def create_make_config(path):
    file_name = "%s/make_config.py" % (path)
    fl = open(file_name, "w")
    fl.write('\ntheme="default"\n')
    fl.write('\nexporter="org-exporter-9"\n')
    proxy = '\nhttp_proxy="%s"\n' % (get_proxy())
    fl.write(proxy)
    fl.close


def set_proxy():
    os.environ["http_proxy"] = get_proxy()
    os.environ["https_proxy"] = get_proxy()


def clone_repo(url, branch):
    repo_full_path = get_repo_full_path(url)
    repo_name = get_dir_of_repo(url)
    if branch == "":
        branch = "master"

    clone_cmd = "git clone -b %s %s %s" % (branch, url, repo_full_path)

    try:
        (ret_code, output) = execute_command(clone_cmd)
    except Exception, e:
        print "Error Cloning the repository: " + str(e)
        raise e


def pull_repo(url, branch):
    repo_full_path = get_repo_full_path(url)
    repo_name = get_dir_of_repo(url)
    if branch == "":
        branch = "master"
    pull_cmd = "git -C %s checkout %s;git -C %s pull origin %s" \
      % (repo_full_path, branch, repo_full_path, branch)

    try:
        (ret_code, output) = execute_command(pull_cmd)
        print "Pull repo successful"
    except Exception, e:
        print "Error Pulling the repository: " + str(e)
        raise e


def fetch_and_build(repo_dict):
    make_infra_path()
    url = repo_dict["url"]
    branch = repo_dict["branch"]
    make_required = repo_dict["make_required"]

    if repo_exists(url):
        pull_repo(url, branch)
    else:
        clone_repo(url, branch)

    if make_required == 1:
        build_repo(url)


def build_repo(url):
    repo_full_path = get_repo_full_path(url)
    create_make_config(repo_full_path)
    cmd = "(cd %s; make -k all)" % (repo_full_path)
    execute_command(cmd)


def wget_and_untar(org_version, url):
    make_infra_path()
    tar_file_name = url.split("/")[-1:][0]
    org_dir = tar_file_name.split(".tar.gz")[0]
    file_save_path = INFRA_LOC
    wget_cmd = "wget -O %s/%s %s" % (file_save_path,
                                     tar_file_name,
                                     url)

    untar_cmd = "tar zxvf %s/%s -C %s" % (file_save_path,
                                          tar_file_name,
                                          file_save_path)

    remove_tar_cmd = "rm -rf %s/%s" % (file_save_path,
                                       tar_file_name)

    remove_org_version_cmd = "rm -rf %s/%s" % (file_save_path,
                                               org_version)
    rename_cmd = "mv %s/%s %s/%s" % (file_save_path,
                                     org_dir,
                                     file_save_path,
                                     org_version)



    if (os.path.isdir("%s/%s" % (file_save_path, org_version))):
        pass
    else:
        execute_command(remove_org_version_cmd)
        execute_command(wget_cmd)
        execute_command(untar_cmd)
        execute_command(remove_tar_cmd)
        execute_command(rename_cmd)


def copy_exporter(exporter):
    cmd = "rsync -a %s/%s/elisp ./" % (INFRA_LOC, exporter)
    execute_command(cmd)


def link_orgs(org_modes):
    for org_mode in org_modes:
        key = org_mode.keys()[0]
        cmd = "ln -sf %s/%s %s" % (INFRA_LOC, key, key)
        execute_command(cmd)


def copy_theme(theme):
    if theme in map(lambda x: x.keys()[0], basic_themes):
        cmd_copy_templates = "rsync -a %s/%s/org-templates ./src/" % (INFRA_LOC, theme)
        cmd_copy_style = "rsync -a %s/%s/style ./src/" % (INFRA_LOC, theme)
        execute_command(cmd_copy_templates)
        execute_command(cmd_copy_style)
    else:
        exp_theme = exp_themes[theme]
        templates_dir = exp_theme["templates"]
        cmd_copy_templates = "rsync -a %s/%s/build/code/ ./src/org-templates/" % (INFRA_LOC, templates_dir)
        execute_command(cmd_copy_templates)

        processes = exp_theme["processes"]
        create_js_dir_cmd = "mkdir -p ./src/style/js"
        execute_command(create_js_dir_cmd)
        for process in processes:
            cmd_copy_process = "rsync -a %s/%s/build/code/*.js ./src/style/js/" % (INFRA_LOC, process)
            execute_command(cmd_copy_process)

        styles = exp_theme["styles"]
        create_css_dir_cmd = "mkdir -p ./src/style/css"
        execute_command(create_css_dir_cmd)
        for style in styles:
            cmd_copy_css = "rsync -a %s/%s/build/code/runtime/css/*.css ./src/style/css/" % (INFRA_LOC, style)
            execute_command(cmd_copy_css)

def clean_local(org_modes):
    rm_style_cmd = "rm -rf ./src/style"
    execute_command(rm_style_cmd)

    rm_templates_cmd = "rm -rf ./src/org-templates"
    execute_command(rm_templates_cmd)

    rm_exporter_cmd = "rm -rf ./elisp"
    execute_command(rm_exporter_cmd)

    rm_build_cmd = "rm -rf ./build"
    execute_command(rm_build_cmd)

    for org_mode in org_modes:
        key = org_mode.keys()[0]
        cmd = "rm -rf ./%s" % (key)
        execute_command(cmd)


def clean_infra():
    rm_cmd = "rm -rf %s" % (INFRA_LOC)
    execute_command(rm_cmd)


def execute_command(cmd):
    set_proxy()
    print "command: %s" % (cmd)
    return_code = -1
    output = None
    # TODO: Checkout alternative os.system(cmd)
    try:
        output = subprocess.check_output(cmd, shell=True)
        return_code = 0
    except subprocess.CalledProcessError as cpe:
        print "Called Process Error Message: %s" % (cpe.output)
        raise cpe
    except OSError as ose:
        print "OSError: %s" % (ose.output)
        raise ose

    return (return_code, output)
